# Convert the following specification into code. Do the point and rectangle ever
# overlap?

# A point starts at [10, 20]. It repeatedly changes position by [3, 0.7] — e.g.,
# under button or timer control. Meanwhile, a rectangle stays in place. Its
# corners are at [50, 50] (upper left), [180, 50] (upper right), [180, 140]
# (lower right), and [50, 140] (lower left).

# To check for overlap, i.e., collision, just run your code and check visually.
# You do not need to implement a point-rectangle collision test. However, we
# encourage you to think about how you would implement such a test.

import simplegui

rectangleUpperLeft = [50, 50]
rectangleUpperRight = [180, 50]
rectangleLowerRight = [180, 140]
rectangleLowerLeft = [50, 140]

pointStart = [10, 20]
vel = [3, 0.7]
time = 0


# Handler to draw on canvas
def draw(canvas):
    canvas.draw_line(rectangleUpperLeft, rectangleUpperRight, 5, "Red")
    canvas.draw_line(rectangleUpperRight, rectangleLowerRight, 5, "Red")
    canvas.draw_line(rectangleLowerRight, rectangleLowerLeft, 5, "Red")
    canvas.draw_line(rectangleLowerLeft, rectangleUpperLeft, 5, "Red")
    
    pointCurrent = [0,0]
    pointCurrent[0] = pointStart[0] + vel[0] * time
    pointCurrent[1] = pointStart[1] + vel[1] * time
    
    canvas.draw_line(pointStart, pointCurrent, 5, "Blue")

def tick():
    global time
    time = time + 1

# Create a frame and assign callbacks to event handlers
frame = simplegui.create_frame("Home", 300, 300)
frame.set_draw_handler(draw)
timer = simplegui.create_timer(1, tick)

# Start the frame animation
frame.start()
timer.start()
