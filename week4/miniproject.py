# Implementation of classic arcade game Pong
# http://www.codeskulptor.org/#user13_DFWUghhyfvrYDVg_3.py

import simplegui
import random
import math

# initialize globals - pos and vel encode vertical info for paddles
WIDTH = 600
HEIGHT = 400       
BALL_RADIUS = 20
PAD_WIDTH = 8
PAD_HEIGHT = 80
HALF_PAD_WIDTH = PAD_WIDTH / 2
HALF_PAD_HEIGHT = PAD_HEIGHT / 2

UP = -2
DOWN = 2
RIGTH = 1
LEFT = -1

left_paddle_pos = HEIGHT / 2
right_paddle_pos = HEIGHT / 2
left_paddle_vel = 0
right_paddle_vel = 0

left_score = 0
right_score = 0

ball_initial_pos = [WIDTH / 2, HEIGHT / 2]
ball_pos = [WIDTH / 2, HEIGHT / 2]
ball_vel = [0, 0]
time = 0

# helper function that spawns a ball by updating the 
# ball's position vector and velocity vector
def ball_init(horizontal_dir):
    global ball_initial_pos, ball_vel, time

    if timer.is_running():
        timer.stop()

    ball_initial_pos = [WIDTH / 2, HEIGHT / 2]
    vertical_dir = random.choice([-1, 1])
    ball_vel = [horizontal_vel * horizontal_dir, vertical_vel * vertical_dir]
    time = 0
    timer.start()

def paddle_init():
    global left_paddle_pos, right_paddle_pos, left_paddle_vel, right_paddle_vel
    left_paddle_pos = HEIGHT / 2
    right_paddle_pos = HEIGHT / 2
    left_paddle_vel = 0
    right_paddle_vel = 0
    
def score_init():
    global left_score, right_score
    left_score = 0
    right_score = 0

def draw_field(c):
    c.draw_line([WIDTH / 2, 0],[WIDTH / 2, HEIGHT], 1, "White")
    c.draw_line([PAD_WIDTH, 0],[PAD_WIDTH, HEIGHT], 1, "White")
    c.draw_line([WIDTH - PAD_WIDTH, 0],[WIDTH - PAD_WIDTH, HEIGHT], 1, "White")

def paddle_out_of_bounds(vel, mov):
    sign = 0
    if mov != 0:
        sign = mov / math.abs(mov)
    return (vel + sign * HALF_PAD_HEIGHT) % HEIGHT == 0
    
def draw_left_paddle(c):
    start_point = [0 + HALF_PAD_WIDTH, left_paddle_pos - HALF_PAD_HEIGHT]
    end_point = [0 + HALF_PAD_WIDTH, left_paddle_pos + HALF_PAD_HEIGHT]
    c.draw_line(start_point, end_point, PAD_WIDTH, "Blue")

def draw_right_paddle(c):
    start_point = [WIDTH - HALF_PAD_WIDTH, right_paddle_pos - HALF_PAD_HEIGHT]
    end_point = [WIDTH - HALF_PAD_WIDTH, right_paddle_pos + HALF_PAD_HEIGHT]
    c.draw_line(start_point, end_point, PAD_WIDTH, "Red")

def draw_ball(c):
    c.draw_circle(ball_pos, BALL_RADIUS, 1, "Yellow", "Yellow")

def draw_score(c):
    c.draw_text(str(left_score), (WIDTH / 2 - 50, 20), 20, "Blue")
    c.draw_text(str(right_score), (WIDTH / 2 + 40, 20), 20, "Red")

def strike_top():
    global time
    ball_initial_pos[0] = ball_pos[0]
    ball_initial_pos[1] = 0 + BALL_RADIUS + 1
    time = 0
    ball_vel[1] = -1 * ball_vel[1]

def strike_bottom():
    global time
    ball_initial_pos[0] = ball_pos[0]
    ball_initial_pos[1] = HEIGHT - BALL_RADIUS - 1
    time = 0
    ball_vel[1] = -1 * ball_vel[1]

def strike_left():
    global time
    ball_initial_pos[0] = 0 + PAD_WIDTH + BALL_RADIUS + 1
    ball_initial_pos[1] = ball_pos[1]
    time = 0
    ball_vel[0] = -1 * ball_vel[0]
    
def strike_right():
    global time
    ball_initial_pos[0] = WIDTH - PAD_WIDTH - BALL_RADIUS - 1
    ball_initial_pos[1] = ball_pos[1]
    time = 0
    ball_vel[0] = -1 * ball_vel[0]
    
def top_bounds():
    return ball_pos[1] - BALL_RADIUS <= 0

def bottom_bounds():
    return ball_pos[1] + BALL_RADIUS >= HEIGHT

def left_bounds():
    return ball_pos[0] - BALL_RADIUS <= PAD_WIDTH

def rigth_bounds():
    return ball_pos[0] + BALL_RADIUS >= WIDTH - PAD_WIDTH

def left_paddle_bounds():
    return (ball_pos[1] >= left_paddle_pos - HALF_PAD_HEIGHT) and (ball_pos[1] <= left_paddle_pos + HALF_PAD_HEIGHT)

def right_paddle_bounds():
    return (ball_pos[1] >= right_paddle_pos - HALF_PAD_HEIGHT) and (ball_pos[1] <= right_paddle_pos + HALF_PAD_HEIGHT)

def left_win():
    global left_score
    left_score = left_score + 1
    ball_init(LEFT)
    
def right_win():
    global right_score
    right_score = right_score + 1
    ball_init(RIGTH)
    
def check_left():
    if left_paddle_bounds():
        strike_left()
    else:
        right_win()

def check_right():
    if right_paddle_bounds():
        strike_right()
    else:
        left_win()

def determinate_game_status():
    if top_bounds():
        strike_top()
    if bottom_bounds():
        strike_bottom()
    if left_bounds():
        check_left()
    if rigth_bounds():
        check_right()

# define event handlers

def new_game():
    paddle_init()
    score_init()
    ball_init(RIGTH)

def new_game_random():
    global horizontal_vel, vertical_vel
    horizontal_vel = random.randrange(6, 24)
    vertical_vel = random.randrange(3, 18)
    new_game()

def new_game_easy():
    global horizontal_vel, vertical_vel
    horizontal_vel = random.randrange(6, 12)
    vertical_vel = random.randrange(3, 9)
    new_game()

def new_game_crazy():
    global horizontal_vel, vertical_vel
    horizontal_vel = random.randrange(12, 24)
    vertical_vel = random.randrange(6, 18)
    new_game()

def draw(c):
    global score1, score2, left_paddle_pos, right_paddle_pos, ball_pos, ball_vel
 
    # update paddle's vertical position, keep paddle on the screen
    if not paddle_out_of_bounds(left_paddle_pos + left_paddle_vel, left_paddle_vel):
        left_paddle_pos = left_paddle_pos + left_paddle_vel
    if not paddle_out_of_bounds(right_paddle_pos + right_paddle_vel, right_paddle_vel):
        right_paddle_pos = right_paddle_pos + right_paddle_vel

    # draw mid line and gutters
    draw_field(c)

    # draw paddles
    draw_left_paddle(c)
    draw_right_paddle(c)

    # update ball
    ball_pos[0] = ball_initial_pos[0] + time * ball_vel[0]
    ball_pos[1] = ball_initial_pos[1] + time * ball_vel[1]
    determinate_game_status()

    # draw ball and scores
    draw_ball(c)
    draw_score(c)
        
def keydown(key):
    if key == simplegui.KEY_MAP["up"]:
        start_right(UP)
    if key == simplegui.KEY_MAP["down"]:
        start_right(DOWN)

    if key == simplegui.KEY_MAP["w"]:
        start_left(UP)
    if key == simplegui.KEY_MAP["s"]:
        start_left(DOWN)
   
def keyup(key):
    if key == simplegui.KEY_MAP["up"]:
        stop_right()
    if key == simplegui.KEY_MAP["down"]:
        stop_right()

    if key == simplegui.KEY_MAP["w"]:
        stop_left()
    if key == simplegui.KEY_MAP["s"]:
        stop_left()

def tick():
    global time
    time = time + 1

def start_left(movement):
    global left_paddle_vel
    left_paddle_vel = movement

def start_right(movement):
    global right_paddle_vel
    right_paddle_vel = movement

def stop_left():
    global left_paddle_vel
    left_paddle_vel = 0

def stop_right():
    global right_paddle_vel
    right_paddle_vel = 0

# create frame
frame = simplegui.create_frame("Pong", WIDTH, HEIGHT)
frame.set_draw_handler(draw)
frame.set_keydown_handler(keydown)
frame.set_keyup_handler(keyup)

btnRestart = frame.add_button("New Game (Random)", new_game_random, 100)
#btnEasy = frame.add_button("New Game (Easy)", new_game_easy, 100)
#btnCrazy = frame.add_button("New Game (Kamikaze)", new_game_crazy, 100)

timer = simplegui.create_timer(100, tick)

# start frame
frame.start()
new_game_random()