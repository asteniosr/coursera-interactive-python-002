# Week1 - miniproject user10_dA6ck2Ep2jegoTn_1.py
# Rock-paper-scissors-lizard-Spock

# The key idea of this program is to equate the strings
# "rock", "paper", "scissors", "lizard", "Spock" to numbers
# as follows:
#
# 0 - rock
# 1 - Spock
# 2 - paper
# 3 - lizard
# 4 - scissors

import random

# helper functions
def number_to_name(number):
    if number == 0:
        return "rock"
    if number == 1:
        return "Spock"
    if number == 2:
        return "paper"
    if number == 3:
        return "lizard"
    if number == 4:
        return "scissors"

    return "ERROR"

    # convert number to a name using if/elif/else
    # don't forget to return the result!

    
def name_to_number(name):
    if name == "rock":
        return 0
    if name == "Spock":
        return 1
    if name == "paper":
        return 2
    if name == "lizard":
        return 3
    if name == "scissors":
        return 4

    return "ERROR"
    # convert name to number using if/elif/else
    # don't forget to return the result!


def rpsls(name): 
    # fill in your code below

    # convert name to player_number using name_to_number
    player_number = name_to_number(name)

    # compute random guess for comp_number using random.randrange()
    comp_number = random.randrange(0, 5);

    # compute difference of player_number and comp_number modulo five
    winner = (player_number - comp_number) % 5

    # use if/elif/else to determine winner
    if winner == 0 :
        winner_name = "Player and computer tie!"
    elif winner < 3:
        winner_name = "Player wins!"
    else :
        winner_name = "Computer wins!"
    
    # convert comp_number to name using number_to_name
    comp_name = number_to_name(comp_number)

    # print results
    print "Player chooses", name
    print "Computer chooses", comp_name
    print winner_name
    print
    
# test your code
rpsls("rock")
rpsls("Spock")
rpsls("paper")
rpsls("lizard")
rpsls("scissors")

# always remember to check your completed program against the grading rubric
