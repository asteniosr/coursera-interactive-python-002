# Quizz 1 - Q7: user10_rnVbCMQ4H4t8SAl_1.py

# There are several ways to calculate the area of a regular polygon. Given the
# number of sides, n, and the length of each side, s, the polygon's area is
#
#                   ¼ n s2 / tan(π/n).
#
# For example, a regular polygon with 5 sides, each of length 7 inches, has
# area 84.30339262885938 square inches.
#
# Write a function that calculates the area of a regular polygon, given the
# number of sides and length of each side. Submit the area of a regular
# polygon with 7 sides each of length 3 inches. Enter a number (and not the
# units) with at least four digits of precision after the decimal point.
#
# Note that the use of inches as the unit of measurement in these examples is
# arbitrary. Python only keeps track of the numerical values, not the units.

import math

def area(num_of_sides, length_of_sides):
    return (1/4) * num_of_sides * length_of_sides ** 2 / math.tan(math.pi / num_of_sides)

print area(7, 3)

print area(5, 7)
print 84.30339262885938
