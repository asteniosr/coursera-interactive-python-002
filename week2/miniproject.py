# Week2 - miniproject user11_c1EeCWODBC_4.py
# "Guess the number"

# input will come from buttons and an input field
# all output for the game will be printed in the console

import random
import simplegui
import math

# initialize global variables used in your code
secret = -1
low = -1
high = -1
remaining_guesses = 0
alert_times_displayed = 0
alert_color = ["Black"]

# helper functions
def set_low(l):
    global low
    low = l

def set_high(h):
    global high
    high = h

def new_secret():
    global secret
    secret = random.randrange(low, high)

def set_max_guesses():
    global remaining_guesses
    remaining_guesses = math.ceil(math.log(high - low + 1, 2))

def print_range():
    return "Guess the number between " + str(low) + " and " + str(high - 1)

def print_remaining_guesses():
    return "Don't worry you still have " + str(remaining_guesses) + " guesses"

def print_max_guesses():
    return "You have " + str(remaining_guesses) + " guesses"

def restart():
    new_secret()
    set_max_guesses()
    lbl_range.set_text(print_range())
    lbl_guesses.set_text(print_max_guesses())
    in_guess.set_text("")
    print "New Game"
    print print_range()
    print print_max_guesses()

def draw_color():
    index = alert_times_displayed % len(alert_color)
    frame.set_canvas_background(alert_color[index])

def show_alert():
    global alert_times_displayed
    if alert_times_displayed == 0:
        alert_timer.stop()
    else:
        draw_color()
        alert_times_displayed = alert_times_displayed - 1

def draw(repeat, color):
    global alert_color
    alert_color = [color, "Black"]
    global alert_times_displayed
    alert_times_displayed = 2 * repeat
    alert_timer.start()

def win():
    draw(5, "Green")
    print "Correct!"
    print
    restart() # new game starts

def higger():
    draw(1, "Orange")
    print "Higher"
    
def lower():
    draw(1, "Purple")
    print "Lower"

def hint_next_guess(iguess):
    if iguess < secret:
        higger()
    else:
        lower()
    lbl_guesses.set_text(print_remaining_guesses())
    print print_remaining_guesses()

def loose():
    draw(5, "Red")
    print "You lose! The number was " + str(secret)
    print
    restart() # new game starts
    
def end_game():
    return remaining_guesses == 0

def set_game_status(iguess):
    global remaining_guesses
    remaining_guesses = remaining_guesses - 1

    if end_game():
        loose()
    else:
        hint_next_guess(iguess)

# define event handlers for control panel
def range100():
    # button that changes range to range [0,100) and restarts
    set_low(0)
    set_high(100)
    restart()

def range1000():
    # button that changes range to range [0,1000) and restarts
    set_low(0)
    set_high(1000)
    restart()
    
def get_input(guess):
    print "Guess was " + guess

    iguess = int(guess)
    if iguess == secret:
        win()
    else:
        set_game_status(iguess)
    
# create frame
frame = simplegui.create_frame("Guess the number", 200, 200, 300)
alert_timer = simplegui.create_timer(200, show_alert)

# register event handlers for control elements
btn_100 = frame.add_button("New game [0,100)", range100, 200)
btn_1000 = frame.add_button("New game [0,1000)", range1000, 200)
lbl_range = frame.add_label(print_range())
lbl_guesses = frame.add_label(print_max_guesses())
in_guess = frame.add_input("Enter a guess", get_input, 200)

# start frame
frame.start()
range100() #starts the game in range [0,100)

# always remember to check your completed program against the grading rubric
