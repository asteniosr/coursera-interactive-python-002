# "Stopwatch: The Game" - /#user12_Eg0LMZ55h1Obj7O_2.py

import simplegui
import time

# define global variables
startTime = 0
stopedTime = 0
elapsedTime = 0
elapsedTenths = ""

wins = 0
tries = 0
score = ""

# define helper function format that converts time
# in tenths of seconds into formatted string A:BC.D
def format(t):
    tenths = t % 10
    seconds = int(t / 10) % 60
    secondsU = seconds % 10
    secondsT = int(seconds / 10)
    minutes = int(t / 600)
    global elapsedTenths
    elapsedTenths = str(minutes) + ":" + str(secondsT) + str(secondsU) + "." + str(tenths)

def set_score():
    global score
    score = str(wins) + "/" + str(tries)

def win():
    return int(stopedTime * 10) % 10 == 0

def update_score():
    global wins, tries
    tries = tries + 1
    if win():
        wins = wins + 1
    set_score()

def init():
    global startTime, stopedTime
    startTime = 0
    stopedTime = 0
    format(0)
    global wins, tries
    wins = 0
    tries = 0
    set_score()

# define event handlers for buttons; "Start", "Stop", "Reset"
def start():
    if timer.is_running():
        return;

    global startTime
    startTime = time.time()
    timer.start()

def stop():
    if not timer.is_running():
        return;
    
    timer.stop()
    global stopedTime
    stopedTime = elapsedTime
    update_score()

def reset():
    timer.stop()
    init()

# define event handler for timer with 0.1 sec interval
def updateTime():
    currentTime = time.time()
    global elapsedTime
    elapsedTime = currentTime - startTime + stopedTime
    format(int(elapsedTime * 10))

# define draw handler
def draw(canvas):
    canvas.draw_text(score, (260, 30), 20, "Green")
    canvas.draw_text(elapsedTenths, (90, 160), 50, "White")
    
# create frame
frame = simplegui.create_frame("Stopwatch: The Game", 300, 300, 50)

# register event handlers
btnStart = frame.add_button("Start", start, 50)
btnStop = frame.add_button("Stop", stop, 50)
btnReset = frame.add_button("Reset", reset, 50)
timer = simplegui.create_timer(100, updateTime)
frame.set_draw_handler(draw)

# start frame
frame.start()
init()

